import tornado.web
from views import Index,interview
import config


#路由 
class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", Index.IndexHandler,),
            (r"/tem", Index.MainHandler,),
            (r"/sina", Index.SinaFirstHandler),
            (r"/md_admin/weibo", Index.SinaBackHandler),
            (r"/upload", Index.UploadHandler),
            (r"/celery", Index.CeleryHandler),
            (r"/tree", Index.TreeHandler),
            (r"/getip", Index.GetIpHandler),
            (r"/page", Index.PayPageHandler),
            (r"/alipayreturn", Index.PayRetrunHandler),
            (r"/maketoken", Index.MakeTokenHandler),
            (r"/websocket", Index.WebHandler),
            (r"/chat", Index.ChatHandler),
            (r"/miaosha", Index.MiaoShaHandler),
            (r"/rpc", Index.TestRpcHandler),
            (r"/insert", interview.InsertHandler),
            (r"/find", interview.FindHandler),
            (r"/com", interview.ComHandler),
            (r"/findcom", interview.FindComHandler),
            (r"/all", interview.AllHandler),
            (r"/chrome", interview.ChromeHandler),
            (r"/uptoken", Index.QiNiuHandler),
        ]
        super(Application,self).__init__(handlers,**config.setting)