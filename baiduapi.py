#请求百度api接口
import requests
import json
import base64
import urllib
import time

mystr = "123123"

if '4' in mystr:
    print('111')

def logged(name=None,version=None):

    def decorate(func):


        print(name,version)
        
        @wraps(func)
        def wrapper(*args, **kwargs):
            local_time = time.time()
            print('current Function [%s] run time is %.2f')% (func.__name__ ,time.time() - local_time) 
            return func(*args, **kwargs)
        return wrapper
    return decorate

# Your Code Here
# data = [1,1,3,4]
# single = 0
# Map = {}
# for i in range(len(data)):
#     if data[i] in Map:
#         Map[data[i]] += 1
#     else:
#         Map[data[i]] = 1
# for key in Map:
#     if Map[key] == 1:
#         single = key
#         break
# return single
# End Your Code

# res = requests.get("https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=X4zQnKcx3oeNP8avL4Qmvrbw&client_secret=jAzondIDW9SoveGxdBd0UFAnw7i3rzXu")

# res = json.loads(str(res.text))
# token = res['access_token']

# temp_url = 'https://aip.baidubce.com/rest/2.0/ocr/v1/accurate_basic?access_token=' + token
# temp_headers = {'Content-Type': 'application/x-www-form-urlencoded'}
# temp_file = open('./4.png', 'rb')
# temp_image = temp_file.read()
# temp_file.close()
# temp_data = {'image': base64.b64encode(temp_image)}
# temp_data = urllib.parse.urlencode(temp_data)
# temp_res = requests.post(url=temp_url, data=temp_data, headers=temp_headers)

# res = json.loads(str(temp_res.text))
# print(res)
# for x in res['words_result']:
#     print(x['words'])

# a1 = []
# with open('1.txt') as fp:  
#     a1 = fp.readlines()

# for i,val in enumerate(a1):
#     a1[i] = a1[i].replace("\n","")


# a4 = []
# with open('4.txt') as fp:  
#     a4 = fp.readlines()

# for i,val in enumerate(a4):
#     a4[i] = a4[i].replace("\n","")

# a3 = []
# with open('3.txt') as fp:  
#     a3 = fp.readlines()

# for i,val in enumerate(a3):
#     a3[i] = a3[i].replace("\n","")

# #在a2中但不在a3中
# # subtraction = list(set(a2).difference(set(a3)))

# # print(subtraction)

# # #在a2中但不在a1中

# subtraction = list(set(a3).difference(set(a2)))

# print(subtraction)


# #
# a5 = ['葛永祥', '李冬凤', '邸银淑', '李海峰', '刘俊薇', '魏鹏绪', '王蓓蓓', '李源', '李洪娟', '刘庆生', '史奕', '李晨', '朱燕', '赵华', '钱中华','姚全', '秦嘉戚', '王建英', '侯红叶', '王志国', '胡艳', '赛娟', '刘娟', '陈大颖', '王焕春', '张海艳', '隋强', '郭欢', '崔娜娟', '刘全喜', '吕素梅', '李云阁', '石磊', '蔡大鑫', '王宁', '高丹枫', '岳沛芬', '赵俊秋', '张伟', '毕错']

# #在5中但不在4中

# subtraction = list(set(a4).difference(set(a5)))
# print(subtraction)