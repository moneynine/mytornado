from  tornado.web import RequestHandler
import tornado.ioloop
import tornado.web

from redis import Redis

class IndexHandler(RequestHandler):
    def get(self):
        redis = Redis(host='redis', port=6379)
        count = redis.incr("mycount")
        self.write('hello world %s ' % count)


def make_app():
    return tornado.web.Application(handlers=[
        (r'/', IndexHandler)
    ],debug=True)


if __name__ == '__main__':
    app = make_app()
    app.listen(8000)
    tornado.ioloop.IOLoop.current().start()