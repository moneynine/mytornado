import psycopg2

import psycopg2.extras

conn = psycopg2.connect(host='localhost', port=6432, user='postgres', password='root', database='mytest')


cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) 

cursor.execute('SELECT * FROM article WHERE id = 1;')

result = cursor.fetchone()

print(result)