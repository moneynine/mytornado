
# 一个列表具有嵌套关系
# b = [0, 1, [2, 3, [4, [5, 6, 7], 8], 9, 10, [[11, 12], [13]], 14, 15], 16]

# print(b)

# 现在需要把里面的嵌套关系弄出来
# 如下：

# s1 = [5, 6, 7]
# s2 = [11, 12]
# s3 = [13]
# s4 = [4, s1 , 8]
# s5 = [s2, s3]
# s6 = [2, 3, s4, 9, 10, s5, 14, 15]

# a = [0, 1, s6, 16]

# print(a)


# mylist =[1,[2],[[3]],[[4,[5],6]],7,8,[9]]

# dic = {}

# def getitem(l,level=0):
#     slist = []
#     for item in l:
#         if isinstance(item,list):
#             getitem(item,level+1)
#         else:
#             for tab in range(level):
#                 print('\t',end='')
#             slist.append(item)
#             dic[level] = slist
#             print(level,item)

# # getitem(mylist)

# # print(dic)

# class MyQueue:
#     def __init__(self):
#         self.s = []
#     def push(self, x: int) -> None:
#         self.s.append(x)
#     def pop(self) -> int:    
#         return self.s.pop(0)
#     def peek(self) -> int:
#         return self.s[0]
#     def empty(self) -> bool:
#         return not bool(self.s)

# myq = MyQueue()
# # myq.push(1)

# print(myq.empty())


# import os

# if __name__ == '__main__':
#     os.environ.setdefault("DJANGO_SETTINGS_MODULE", "orm02.settings")
#     import django
#     django.setup()
    
# 	from app01 import models
#     ret = models.Book.objects.all().values('title')
#     print(ret)

# import threading
# balance = 0
# def change_it_without_lock(n):
#     global balance
#     # 不加锁的话 最后的值不是0
#     # 线程共享数据危险在于 多个线程同时改同一个变量
#     # 如果每个线程按顺序执行，那么值会是0， 但是线程时系统调度，又不确定性，交替进行
#     # 没锁的话，同时修改变量
#     # 所以加锁是为了同时只有一个线程再修改，别的线程表一定不能改
#     if lock.acquire():
#         try:
#             for i in range(1000000):
#                 balance = balance + n
#                 balance = balance - n
#         finally:
#             lock.release()

# lock = threading.Lock()

# threads = [
#     threading.Thread(target=change_it_without_lock, args=(8,) ),
#     threading.Thread(target=change_it_without_lock, args=(10,) )
# ]

# [t.start() for t in threads]

# print(balance)

class Meta(type):
    def __new__(cls, name, bases, dct):
        x = super().__new__(cls, name, bases, dct)
        x.attr = 100
        return x


class Foo(metaclass=Meta):
    pass

print(Foo.attr)









