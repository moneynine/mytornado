service PingPong {
    string ping(),
    string check_login(
        1: string username,
        2: string password
    ),
}