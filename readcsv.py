import pandas as pd 
import re 



#用pandas倒入csv文件

df = pd.read_csv(open(r'./orders_export.csv'),encoding='utf_8_sig',) 
#重组csv文件内容
list = []
#读取出每一行数据
for row in df.iterrows():
    data = {}
    name = row[1]["Name"]
    #data["*订单号"] = re.findall("#(.+)",name)[0]
    data["*订单号"] = name.replace("#",'')
    data["*sku"] = "test1"                           #需要修改
    data["属性(可填写SKU尺寸、颜色等)"] = "25"           #需要修改
    if row[1]["Subtotal"] == None:
        data["数量"] = row[1]["Lineitem price"]/row[1]["Lineitem price"]
    elif 1<float(row[1]["Subtotal"]/row[1]["Lineitem price"])<=2:
        data["数量"] = 2
    else:
        data["数量"] = row[1]["Subtotal"]/row[1]["Lineitem price"]
    data["单价"] = row[1]["Lineitem price"]
    data["币种（默认USD）"] = row[1]["Currency"]
    data["*买家姓名"] = row[1]["Billing Name"]
    data["*地址1"] = row[1]["Billing Address1"]
    data["地址2"] = row[1]["Billing Address1"]
    data["*城市"] = row[1]["Billing City"]
    data["*省/州"] = row[1]["Shipping Province"]
    data["*国家二字码"] = row[1]["Billing Country"]
    data["*邮编"] = row[1]["Billing Zip"]       # 这个字段有待修改
    data["电话"] = row[1]["Billing Phone"]
    data["手机"] = row[1]["Shipping Phone"]
    data["E-mail"] = row[1]["Email"]
    data["税号"] = None
    data["门牌号"] = None
    data["公司名"] = None
    data["订单备注"] = None
    data["图片网址"] = None
    data["售出链接"] = None
    data["中文报关名"] = None
    data["英文报关名"] = None
    data["申报金额（USD）"] = None
    data["申报重量（g）"] = None
    data["海关编码"] = None
    data["报关属性"] = None

    print(data)
    list.append(data)


df1=pd.DataFrame(list)
df1.to_excel('abcd.xlsx',index = False)
    






