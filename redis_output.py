from redis_queue import RedisQueue
import time
import threading


def dojob():

    q = RedisQueue('rq')
    while 1:
        result = q.get_nowait()
        if not result:
            break
        print(result)
        time.sleep(2)

for index in range(2):

        thread = threading.Thread(target=dojob)
        thread.start()