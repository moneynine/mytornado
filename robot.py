#EC90485937c351bfaed41fea8eda5f1e155bbf22842d5f9d6871999e05822fd894


import time
import hmac
import hashlib
import base64
import urllib.parse

timestamp = str(round(time.time() * 1000))
secret = 'SEC575ce17d343c24992d3b04277f6805ab2b5b1f09a876f0be6f6d7efb0719cffa'
secret_enc = secret.encode('utf-8')
string_to_sign = '{}\n{}'.format(timestamp, secret)
string_to_sign_enc = string_to_sign.encode('utf-8')
hmac_code = hmac.new(secret_enc, string_to_sign_enc, digestmod=hashlib.sha256).digest()
sign = urllib.parse.quote(base64.b64encode(hmac_code))
# print(timestamp)
# print(sign)


#https://oapi.dingtalk.com/robot/send?access_token=372c7da9a628572e9aa1281a6011cb4bcd42a503e85ab67625c247171862ffd2

import requests,json   #导入依赖库
headers={'Content-Type': 'application/json'}   #定义数据类型
webhook = 'https://oapi.dingtalk.com/robot/send?access_token=372c7da9a628572e9aa1281a6011cb4bcd42a503e85ab67625c247171862ffd2&timestamp='+timestamp+"&sign="+sign
#定义要发送的数据
#"at": {"atMobiles": "['"+ mobile + "']"
data = {
    "msgtype": "text",
    "text": {"content": '有人没做完么，没做完的末班'},
    "isAtAll": True}
res = requests.post(webhook, data=json.dumps(data), headers=headers)   #发送post请求

print(res.text)