from PIL import Image
# img = Image.open('logo.png').convert('RGB')
# img.save('logo.webp', 'webp')

import os

files = os.listdir()

images = [file for file in files if file.endswith(('jpg','png','jpeg'))]

print(images)


def convert_image(image_path, image_type):

    im = Image.open(image_path)
    print(image_path)
    im = im.convert('RGB')
    image_name = image_path.split('.')[0]
    print(f"This is the image name: {image_name}")

    if not os.path.exists(f"{image_path}.webp"):

        if image_type == 'jpg' or image_type == 'png' or image_type == 'jpeg':
            im.save(f"{image_name}.{image_type}.webp", 'webp')
        else:
            raise Error


for image in images:
    if image.endswith('jpg'):
        convert_image(image, image_type='jpg')
    elif image.endswith('jpeg'):
        convert_image(image, image_type='jpg')
    elif image.endswith('png'):
        convert_image(image, image_type='png')
    else:
        raise Error