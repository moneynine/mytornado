from tornado.web import RequestHandler

class BaseHandler(RequestHandler):

    def prepare(self):
        print('提前工作')

    def set_default_headers(self):
        print("setting headers!!!")
        self.set_header("Access-Control-Allow-Origin", "*") # 这个地方可以写域名
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST,GET,OPTIONS,PUT,DELETE')

    def on_finish(self):
        print('善后工作')